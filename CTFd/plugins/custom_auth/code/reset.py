import base64
import flask
import itsdangerous
import CTFd

from . import tools

def handle(data=None):
    # Temporary values
    RESET_PAGE = "reset_page.html"
    infos = CTFd.utils.helpers.get_infos()
    errors = CTFd.utils.helpers.get_errors()
    # Check if data was sent
    if data:
        # Validate data
        try:
            email_address = CTFd.utils.security.signing.unserialize(data, max_age=1800)
        except (itsdangerous.exc.BadTimeSignature, itsdangerous.exc.SignatureExpired, base64.binascii.Error, TypeError):
            CTFd.models.db.session.close()
            return flask.redirect("/")
        # Handle GET request
        if flask.request.method == "GET":
            CTFd.models.db.session.close()
            return flask.render_template(RESET_PAGE, infos=infos, errors=errors, mode="password")
        # Retrieve form data
        password = flask.request.form.get("password", "").strip()
        # Retrieve CTFd account
        account = CTFd.models.Users.query.filter_by(email=email_address).first()
        # Validate form data
        errors.extend(tools.validate_strong_password(password))
        # Check for errors
        if len(errors) > 0:
            CTFd.models.db.session.close()
            return flask.render_template(RESET_PAGE, infos=infos, errors=errors, mode="password")
        # Change password
        account.password = password
        CTFd.models.db.session.commit()
        # Send email
        CTFd.utils.email.password_change_alert(account.email)
        # Go to index page
        CTFd.models.db.session.close()
        return flask.redirect("/")
    else:
        # Check if email system is disabled
        if CTFd.utils.config.can_send_mail() is False:
            errors.append("Email system not enabled, contact the administrator!")
            CTFd.models.db.session.close()
            return flask.render_template(RESET_PAGE, infos=infos, errors=errors, mode="email")
        # Handle GET request
        if flask.request.method == "GET":
            CTFd.models.db.session.close()
            return flask.render_template(RESET_PAGE, infos=infos, errors=errors, mode="email")
        # Retrieve form data
        email_address = flask.request.form["email"].strip()
        # Retrieve CTFd account
        account = CTFd.models.Users.query.filter_by(email=email_address).first()
        # Check if account exists
        if account:
            # Check if account is from organization
            if account.affiliation != "Unknown":
                errors.append("You cannot change an organizational account!")
                CTFd.models.db.session.close()
                return flask.render_template(RESET_PAGE, infos=infos, errors=errors, mode="email")
            # Send email
            CTFd.utils.email.forgot_password(email_address)
        # Notify user
        infos.append("Check your email to reset your password!")
        CTFd.models.db.session.close()
        return flask.render_template(RESET_PAGE, infos=infos, errors=errors, mode="email")