# Configuration Upload

In order to upload container-based challenges to CTFd, the container challenges plugin must be configured correctly. There are 2 steps that need to be taken:

1. Upload Gitlab registry username
2. Upload Gitlab registry password

## 1. Gitlab Registry Username/Password

The container challenges plugin needs to be able to access your Gitlab challenge images. However, the Gitlab registry is private so it needs credentials to gain access. Gitlab registry credentials are in the form of a username and deploy token.

1. Go to the [Gitlab registry](https://gitlab.com/hu-hc/jcr/challenges) for JCR challenges
2. Go to "Settings"
3. Go to "Repository"
4. Go to "Deploy Tokens"
5. Fill in the "Name"
6. Fill in the "Username" (**this is the username!**)
7. Tick the "read_registry" box
8. Click on "Create deploy token"
9. Copy and write down the displayed token (**this is the password!**)
